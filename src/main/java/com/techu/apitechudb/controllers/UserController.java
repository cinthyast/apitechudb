package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechudb/v2")

public class UserController
{
    @Autowired
    UserService userService;


    //Consulta de Usuarios
    @GetMapping("/user")
    public  ResponseEntity<List<UserModel>> getUser()
    {
        System.out.println("getUser");
        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);

    }
    //Consulta usuarios ordenado por Edad
    @GetMapping("/user/usersort")
    public  ResponseEntity<List<UserModel>> getUserSort()
    {
        System.out.println("getUserSort");
        return new ResponseEntity<>(this.userService.findAllSort(), HttpStatus.OK);

    }



    //Otra consulta sot
     @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam (name = "$orderby", required=false) String orderBy){
        System.out.println("getUsers ");
        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }

     //Busqueda de usuarios por id
    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id)
    {
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es "+  id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    //Alta de usuarios
    @PostMapping("/user")
    //public ResponseEntity <ProductModel> addProduct(@RequestBody ProductModel product)
    public ResponseEntity <UserModel> addUser(@RequestBody UserModel user)
    {
        System.out.println("addUser");
        System.out.println("La id del usuario a a crear es "+ user.getId());
        System.out.println("el nombre del usuario a crear es "+ user.getName());
        System.out.println("la edad del usuario a crear es "+ user.getAge());

        return new ResponseEntity<>(this.userService.addUser(user),HttpStatus.CREATED);

    }

    //Borrado
    @DeleteMapping("/user/{id}")
    public ResponseEntity <String> deleteUser(@PathVariable String id)
    {
        System.out.println("deleteUser");
        System.out.println("La id del user a borrar es "+ id);

        boolean deleteUser = this.userService.delete(id);
        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //Modificación de productos
    @PutMapping("/user/{id}")
    public ResponseEntity <UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id)
    {
        System.out.println("updateUser");
        System.out.println("La id del usuario a actualizar en parámetro URL es "+ id);
        System.out.println("La id del usuario es  "+ user.getId());
        System.out.println("El name del usuarjo a actualizar es "+ user.getName());
        System.out.println("La edad del usuario a actualizar es "+ user.getAge());

        //Optional <ProductModel> productToUpdate = this.productService.findById(id);
        Optional <UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent())
        {
            System.out.println("Usuario para actualizar encontrado, actualizando");
            this.userService.update(user);
        }

        return new ResponseEntity<>
                (
                        user,
                        userToUpdate.isPresent() ? HttpStatus.OK:HttpStatus.NOT_FOUND
                );
    }


}
