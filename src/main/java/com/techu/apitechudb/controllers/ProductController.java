package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechudb/v2")
public class ProductController
{
    @Autowired
    ProductService productService;

    //Consulta de productos
    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducs()
    {
        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    //Busqueda de productos por id
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id)
    {
        System.out.println("getProductsById");
        System.out.println("La id del producto a buscar es "+  id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                        result.isPresent() ? result.get() : "Producto no encontrado",
                        result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND
                );
    }

    //Alta de productos
    @PostMapping("/products")
    public ResponseEntity <ProductModel> addProduct(@RequestBody ProductModel product)
    {
        System.out.println("addProducts");
        System.out.println("La id del producto a crear es "+ product.getId());
        System.out.println("La descripción  del producto a crear es "+ product.getDesc());
        System.out.println("El precio del producto a crear es "+ product.getPrice());

        return new ResponseEntity<>(this.productService.addProduct(product), HttpStatus.CREATED);
    }

    //Modificación de productos
    @PutMapping("/products/{id}")
    public ResponseEntity <ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id)
    {
        System.out.println("updateProducts");
        System.out.println("La id del producto a actualizar en parámetro URL es "+ id);
        System.out.println("La id del producto es  "+ product.getId());
        System.out.println("La descripción  del producto a actualizar es "+ product.getDesc());
        System.out.println("El precio del producto a actualizar es "+ product.getPrice());

        Optional <ProductModel> productToUpdate = this.productService.findById(id);
        if (productToUpdate.isPresent())
        {
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productService.update(product);
        }

        //return new ResponseEntity<>(product, HttpStatus.OK);
        return new ResponseEntity<>
                (
                        product,
                        productToUpdate.isPresent() ? HttpStatus.OK:HttpStatus.NOT_FOUND
                );
    }

    //Borrado
    @DeleteMapping("/products/{id}")
    public ResponseEntity <String> deleteProduct(@PathVariable String id)
    {
        System.out.println("deleteProducts");
        System.out.println("La id del producto a borrar es "+ id);

        boolean deleteProduct = this.productService.delete(id);
        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no borrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
