package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService
{
    @Autowired
    ProductRepository productRepository;
//Lista
    public List<ProductModel> findAll()
    {
        System.out.println("findAll en ProductService");
        return this.productRepository.findAll();
    }
//Busqueda
    public Optional<ProductModel> findById(String id)
    {
        System.out.println("findById en ProductService");
        System.out.println("El producto a buscar es "+id);


        return this.productRepository.findById(id);
    }

    //Creación
    public ProductModel addProduct(ProductModel product)
    {
        System.out.println("addProduct en ProductService");

        return this.productRepository.save(product);
    }

    //Modificación
    public ProductModel update(ProductModel product)
    {
        System.out.println("update en ProductService");

        //return this.addProduct(product);
        return this.productRepository.save(product);
    }

    //Borrando
    public boolean delete(String id)
    {
        System.out.println("delete en ProductService");
        boolean result = false;

        if (this.findById(id).isPresent() == true)
        {
            System.out.println("Producto encontrado, borrando "+ id);
            this.productRepository.deleteById(id);
            result = true;
        }
        return result;
    }

    //Patch


}
