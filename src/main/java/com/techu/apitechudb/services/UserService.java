package com.techu.apitechudb.services;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService
{

    @Autowired
    UserRepository userRepository;
    //Lista

    public List<UserModel> findAll()
    {
        System.out.println("findAll en UserService");
        //return this.userRepository.findAll(sortByIdAsc());
        //return this.userRepository.findAll(Sort.sort());
        return this.userRepository.findAll();
        // return studentDao.findAll(sortByIdAsc());
    }

//Lista ordenada
    public List<UserModel> findAllSort()
    {
        System.out.println("findAge en UserService");
        //return this.userRepository.findAll();
        return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
    }

    //Lista ordenada2
/*
    public List<UserModel> findAll(String orderBy)
    {

        System.out.println("getUsers ordenado");
        List<UserModel> result;
        if (orderBy != null)
        {
            System.out.println("SE ha pedido ordenación");
            result=this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));

        }
        else
        {
            result = this.userRepository.findAll();
        }
        return result;
    }
*/
    public List<UserModel> findAll(String orderBy) {
        System.out.println("findAll en  User Service");
        List <UserModel> result;
        if(orderBy!=null){
            result=this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
        }
        else {
            result = this.userRepository.findAll();
        }
        return result;
    }



    //Busqueda
    public Optional<UserModel> findById(String id)
    {
        System.out.println("findById en UserService");
        System.out.println("El usuario a buscar es "+id);


        return this.userRepository.findById(id);
    }

    //Creación
    public UserModel addUser(UserModel user)
    {
        System.out.println("addUser en UserService");

        return this.userRepository.save(user);
    }

    //Modificación
    //public ProductModel update(ProductModel product)
    public UserModel update(UserModel user)
    {
        System.out.println("update en UserService");

        //return this.addProduct(product);
        return this.userRepository.save(user);
    }

    //Borrando
    public boolean delete(String id)
    {
        System.out.println("delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true)
        {
            System.out.println("usuario encontrado, borrando "+ id);
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }

}
